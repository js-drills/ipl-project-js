// ?? extra runs conceedded in 2016

let parse = require("csv-parser");
let fs = require("fs");
const path = require("path");

const matchesdata = path.join(__dirname, "../data/matches.csv");
const deliveriesdataFile = path.join(__dirname, "../data/deliveries.csv");
const dumpPath = path.join(
  __dirname,
  "../public/output/extraRunsConceded.JSON"
);

let extraRunsConceded = () => {
  const readStream = fs.createReadStream(matchesdata);

  let matchid2016 = [];
  let extrarunsperteam = {};
  readStream
    .pipe(parse())
    .on("data", (matchdata) => {
      try {
        if (matchdata.season === "2016") {
          const team = matchdata.winner;
          matchid2016.push(matchdata.id);

          if (!extrarunsperteam[team]) {
            extrarunsperteam[team] = 0;
          }
        }
      } catch (error) {
        console.log(error);
      }
    })
    .on("error", (error) => {
      console.log(error);
    })
    .on("end", () => {
      const readingdeliveries = fs.createReadStream(deliveriesdataFile);

      readingdeliveries
        .pipe(parse())
        .on("data", (deliveriesdata) => {
          try {
            if (matchid2016.includes(deliveriesdata.match_id)) {
              const bowlingTeam = deliveriesdata.bowling_team; // Assuming you have a property named 'bowling_team' in deliveries.csv

              if (!extrarunsperteam[bowlingTeam]) {
                extrarunsperteam[bowlingTeam] = 0;
              }

              extrarunsperteam[bowlingTeam] += parseInt(
                deliveriesdata.extra_runs
              );
            }
          } catch (error) {
            console.log(error);
          }
        })
        .on("error", (error) => {
          console.log(error);
        })
        .on("end", () => {
          try {
            fs.writeFile(
              dumpPath,
              JSON.stringify(extrarunsperteam),
              (error) => {
                if (error) {
                  console.log(error);
                }
              }
            );
          } catch (error) {
            console.log(error);
          }
        });
    });
};

module.exports = extraRunsConceded;
