let fs = require("fs");
const parse = require("csv-parser");
const { error } = require("console");
const path = require("path");

let deliveriesdataFile = path.join(__dirname, '../data/deliveries.csv')
let dumpPath = path.join(__dirname,'../public/output/mostDismmisedBAtsman.JSON')

let mostDismissedBatsmenAndBowler = () => {
let readStream = fs.createReadStream(deliveriesdataFile);

let dismissal_data = {};

readStream
  .pipe(parse())
  .on("data", (matchdetails) => {
    try {
      if (
      matchdetails["player_dismissed"] !== undefined &&
      matchdetails["player_dismissed"] !== ""
    ) {
      if (!dismissal_data[matchdetails["player_dismissed"]]) {
        dismissal_data[matchdetails["player_dismissed"]] = {};
      }

      const bowler = matchdetails.bowler;
      if (dismissal_data[matchdetails["player_dismissed"]][bowler]) {
        dismissal_data[matchdetails["player_dismissed"]][bowler]++;
      } else {
        dismissal_data[matchdetails["player_dismissed"]][bowler] = 1;
      }
    }
    } catch (error) {
      console.log(error)
    }
  }).on('error', (error) => {
    console.log(error)
  })
  .on("end", () => {
    try {
      let mostDismissedBatsmen = Object.fromEntries(
      Object.entries(dismissal_data).map((x) => {
        const key = x[0];
        const values = Object.entries(x[1])
          .sort((a, b) => {
            return b[1] - a[1];
          })
          .slice(0, 1)
          .flat(); // flatten the inner array
        return [key, { [values[0]]: values[1] }];
      })
    );
    mostDismissedBatsmen = Object.fromEntries(
      Object.entries(mostDismissedBatsmen).sort((a, b) => {
        return Object.entries(b[1])[0][1] - Object.entries(a[1])[0][1];
      })
    );

      fs.writeFile(dumpPath, JSON.stringify(mostDismissedBatsmen), (error) => {
        if (error) {
        console.log(error)
      }
    })
    } catch (error) {
      console.log(error)
    }
  });

}

module.exports = mostDismissedBatsmenAndBowler