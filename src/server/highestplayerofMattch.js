// highestPlayer of the match

let fs = require("fs");
let parse = require("csv-parser");
let path = require("path");


let matchesFile = path.join(__dirname, "../data/matches.csv");
const dumpPath = path.join(
  __dirname,
  "../public/output/heighestPlayerOfMatch.JSON"
);

let heighestPlayerOfMatch = (findMostRepeatedElement) => {
  let season_Players = {};

  const readStream = fs.createReadStream(matchesFile);
  readStream
    .pipe(parse())
    .on("data", (matches) => {
      if (!season_Players[matches.season]) {
        season_Players[matches.season] = [];
      } else {
        season_Players[matches.season].push(matches.player_of_match);
      }
    })
    .on("error", (error) => {
      console.log(error);
    })
    .on("end", () => {
      try {
        let result = findMostRepeatedElement(season_Players);
        fs.writeFile(dumpPath, JSON.stringify(result), (error) => {
          if (error) {
            console.log(error);
          }
        });
      } catch (error) {
        console.log(error);
      }
    });
};

// for finding most repeated from the obj we have.
function findMostRepeatedElement(obj) {
  if (typeof obj == "object") {
    let sean_playerOfMatch = [];

    for (let key in obj) {
      let mostRepeated = { year: null, batsman: null, count: 0 };
      if (obj.hasOwnProperty(key)) {
        let counts = {};
        for (let value of obj[key]) {
          counts[value] = (counts[value] || 0) + 1;
          if (counts[value] > mostRepeated.count) {
            mostRepeated = { year: key, batsman: value, count: counts[value] };
          }
        }
      }
      sean_playerOfMatch.push(mostRepeated);
    }

    return sean_playerOfMatch;
  }
  return null;
}

module.exports = {heighestPlayerOfMatch,findMostRepeatedElement}