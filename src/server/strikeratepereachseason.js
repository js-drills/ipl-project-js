let fs = require("fs");
let parse = require("csv-parser");
const path = require("path");
const { error } = require("console");

let matchesFile = path.join(__dirname, "../data/matches.csv");
let deliveriesdataFile = path.join(__dirname, "../data/deliveries.csv");
let dumpPath = path.join(
  __dirname,
  "../public/output/strikeRatePerEachSeason.JSON"
);

let strikeratePerEachSeason = () => {
  let season_Ids = {};
  let seaon_batsmanballs = {};
  const readStream = fs.createReadStream(matchesFile);
  readStream
    .pipe(parse())
    .on("data", (match) => {
      if (!season_Ids[match.season]) {
        season_Ids[match.season] = [];
      } else {
        season_Ids[match.season].push(match.id);
      }
    })
    .on("end", () => {
      try {
        const readingstrikerate = fs.createReadStream(deliveriesdataFile);
        readingstrikerate
          .pipe(parse())
          .on("data", (deliveriesdata) => {
            for (let key in season_Ids) {
              if (
                season_Ids.hasOwnProperty(key) &&
                season_Ids[key].includes(deliveriesdata.match_id)
              ) {
                if (seaon_batsmanballs[key]) {
                  if (!seaon_batsmanballs[key][deliveriesdata.batsman]) {
                    seaon_batsmanballs[key][deliveriesdata.batsman] = {
                      runs: parseInt(deliveriesdata.batsman_runs),
                      balls: 1,
                    };
                  } else {
                    seaon_batsmanballs[key][deliveriesdata.batsman].runs +=
                      parseInt(deliveriesdata.batsman_runs);
                    seaon_batsmanballs[key][deliveriesdata.batsman].balls++;
                  }
                } else {
                  seaon_batsmanballs[key] = {};
                }
              }
            }
          })
          .on("end", () => {
            for (let key in seaon_batsmanballs) {
              for (let batsman in seaon_batsmanballs[key]) {
                if (!seaon_batsmanballs[key][batsman].strikerate) {
                  // Add the strikerate property to the existing batsman object
                  seaon_batsmanballs[key][batsman] = {
                    strikerate:
                      (seaon_batsmanballs[key][batsman].runs /
                        seaon_batsmanballs[key][batsman].balls) *
                      100,
                  };
                }
              }
            }
            
            // using Reduce 
            // const updatedSeasonBatsmanBalls = Object.entries(
            //   seaon_batsmanballs
            // ).reduce((accumulator, [key, value]) => {
            //   value = Object.fromEntries(
            //     Object.entries(value).map(([keys, values]) => [
            //       keys,
            //       { strikeRate: (values.runs / values.balls) * 100 },
            //     ])
            //   );
            //   // console.log(value);
            //   accumulator[key] = value;
            //   return accumulator;
            // }, {});


            fs.writeFile(
              dumpPath,
              JSON.stringify(seaon_batsmanballs),
              (error) => {
                if (error) {
                  console.log(error);
                }
              }
            );
          });
      } catch (error) {
        console.log(error);
      }
    });
};

module.exports = strikeratePerEachSeason;
