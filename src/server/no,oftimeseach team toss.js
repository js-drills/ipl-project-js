// Find the number of times each team won the toss and also won the match

let fs = require("fs");
let parse = require("csv-parser");
// const { error } = require("console");
const path = require("path");

let matchesFile = path.join(__dirname, '../data/matches.csv')
let dumpPath = path.join(__dirname,'../public/output/tossWon.JSON')

let tossWon = () => {
  let teamswontoss_Match = [];
  let matchescount = {};
  let readStream = fs.createReadStream(matchesFile);

  readStream
    .pipe(parse())
    .on("data", (matches) => {
      if (matches.toss_winner === matches.winner) {
        teamswontoss_Match.push(matches.winner);
      }
    })
    .on("error", (error) => {
      console.log(error);
    })
    .on("end", () => {
      try {
        teamswontoss_Match.forEach((team) => {
          if (!matchescount[team]) {
            matchescount[team] = 1; // Initialize count to 1 for the first match
          } else {
            matchescount[team]++;
          }
        });

        fs.writeFile(dumpPath, JSON.stringify(matchescount), (error) => {
          if (error) {
            console.log(error);
          }
        });
      } catch (error) {
        console.log(error);
      }
    });
};

module.exports = tossWon;
