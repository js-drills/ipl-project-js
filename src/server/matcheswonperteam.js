const { error } = require("console");
const parse = require("csv-parser");
const fs = require("fs");
const path = require("path");

let matchesFile = path.join(__dirname, '../data/matches.csv')
let dumpPath = path.join(__dirname,'../public/output/matcheswonPerteam.JSON')

let matchesWonPerTeam = () => {
  
const matches = {};
let result = [];
const readStream = fs.createReadStream(matchesFile);

readStream
  .pipe(parse())
  .on("data", (matchdata) => {
    try {
      const team = matchdata.team1;
      result.push(matchdata);
      if (!matches[team]) {
        matches[team] = {};
      }
      teams = Object.keys(matches);
    } catch (error) {
      console.log(error);
    }
  }).on('error', (error) => {
    console.log(error)
  })
  .on("end", () => {
    try {
      result.forEach((match) => {
        teams.forEach((team) => {
          if (team === match.winner) {
            if (!matches[team][match.season]) {
              matches[team][match.season] = 1;
            } else {
              matches[team][match.season]++;
            }
          }
        });
      });
      fs.writeFile(dumpPath, JSON.stringify(matches), (error) => {
        if (error) {
          console.log(error);
        }
      });
    } catch (error) {
      console.log(error);
    }
  });
}


module.exports = matchesWonPerTeam