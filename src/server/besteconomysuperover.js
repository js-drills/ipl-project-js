let fs = require("fs");
let parse = require("csv-parser");
const path = require("path");

const deliveriespath = path.join(__dirname, "../data/deliveries.csv");
const dumpPath = path.join(
  __dirname,
  "../public/output/bestSuperOverEconomy.JSON"
);

let readStream = fs.createReadStream(deliveriespath);

const bestSuperOverEconomy = () => {
  let data = {};
  let matchdata = [];
  let mineconomy = 0;
  let bowlername = "";
  readStream
    .pipe(parse())
    .on("data", (deliveriesdat) => {
      try {
        if (deliveriesdat["is_super_over"] > 0) {
          const bowler = deliveriesdat["bowler"];
          data[bowler] = !data.hasOwnProperty(bowler)
            ? { balls: 1, runs: parseInt(deliveriesdat["total_runs"]) }
            : {
                balls: ++data[bowler]["balls"],
                runs:
                  data[bowler]["runs"] + parseInt(deliveriesdat["total_runs"]),
              };
        }
      } catch (error) {
        console.log(error);
      }
    })
    .on("error", (error) => {
      console.log(error);
    })

    .on("end", () => {
      try {
        let economy = Object.entries(data).reduce(
          (accumulator, [key, value]) => {
            accumulator[key] = {
              economy: value["runs"] / (value["balls"] / 6),
            };
            return accumulator;
          },
          {}
        );
        let minEconomyBowler = Object.entries(economy).reduce(
          (accumulator, [key, value]) => {
            let bowlername = key;
            let currentEconomy = value["economy"];

            if (currentEconomy < accumulator.economy) {
              return {
                bowlername: bowlername,
                economy: currentEconomy,
              };
            } else {
              return accumulator;
            }
          },
          { bowlername: null, economy: Infinity }
        );
        fs.writeFile(dumpPath, JSON.stringify(minEconomyBowler), (error) => {
          if (error) {
            console.log(error);
          }
        });
      } catch (error) {
        console.log(error);
      }
    });
};

module.exports = bestSuperOverEconomy;
