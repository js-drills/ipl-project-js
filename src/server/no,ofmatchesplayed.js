const fs = require("fs");
const csv = require("csv-parser");
const { error } = require("console");
const path = require("path");
let matchesFile = path.join(__dirname, '../data/matches.csv')
let dumpPath = path.join(__dirname,'../public/output/noOfMatchesPlayed.JSON')

let noOfMatchesPlayed = () => {
  const result = [];

  const readStream = fs.createReadStream(matchesFile);

  readStream
    .pipe(csv())
    .on("data", (data) => {
      result.push(data);
    }).on('error', (error) => {
      console.log(error)
    }
    ).on("end", () => {
      try {
        let seasons = {};
        result.forEach((match) => {
          if (seasons[match.season]) {
            seasons[match.season] += 1;
          } else {
            seasons[match.season] = 1;
          }
        });
        fs.writeFile(dumpPath, JSON.stringify(seasons), (error) => {
          if (error) {
            console.log(error);
          }
        });
      } catch (error) {
        console.log(error);
      }
    });
}

module.exports = noOfMatchesPlayed