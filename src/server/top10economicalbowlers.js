let fs = require("fs");
let parse = require("csv-parser");

const path = require("path");
const { error } = require("console");

let matchesFile = path.join(__dirname, "../data/matches.csv");
let deliveriesdataFile = path.join(__dirname, "../data/deliveries.csv");
let dumpPath = path.join(
  __dirname,
  "../public/output/top10EconomicalBowler.JSON"
);


let top10EconomicalBowler = () => {
  let matchId2015 = [];
  let bowlereconomy = {};
  let totalballs = {};
  let totalruns = {};

  const readStream = fs.createReadStream(matchesFile);

  readStream
    .pipe(parse())
    .on("data", (matchdata) => {
      if (matchdata.season === "2015") {
        matchId2015.push(matchdata.id);
      }
    }).on('error', (error) => {
      if (error) {
        console.log(error)
      }
    })
    .on("end", () => {
      try {
        const readingbowlerruns = fs
        .createReadStream(deliveriesdataFile)
        .pipe(parse())
        .on("data", (deliveriesdata) => {
          if (matchId2015.includes(deliveriesdata.match_id)) {
            if (!bowlereconomy[deliveriesdata.bowler]) {
              bowlereconomy[deliveriesdata.bowler] = true;
              totalballs[deliveriesdata.bowler] = 0;
              totalruns[deliveriesdata.bowler] = 0;
            }

            totalruns[deliveriesdata.bowler] += parseInt(
              deliveriesdata.total_runs
            );

            if (deliveriesdata.ball < 7) {
              totalballs[deliveriesdata.bowler]++;
            }
          }
        })
        .on("error", (error) => {
          if (error) {
            console.log(error);
          }
        })
        .on("end", () => {
          for (let bowler in bowlereconomy) {
            bowlereconomy[bowler] =
              parseFloat(totalruns[bowler] / totalballs[bowler]) * 6;
          }
          bowlereconomy = Object.fromEntries(
            Object.entries(bowlereconomy)
              .sort((a, b) => a[1] - b[1])
              .slice(0, 10)
          );
          fs.writeFile(dumpPath, JSON.stringify(bowlereconomy), (error) => {
            if (error) {
              console.log(error)
            }
          })
        });
 
      } catch (error) {
        console.log(error)
      }
       });

}

module.exports = top10EconomicalBowler